-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Апр 04 2016 г., 13:31
-- Версия сервера: 5.5.47-0+deb8u1
-- Версия PHP: 5.6.17-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `campaing`
--

CREATE TABLE IF NOT EXISTS `campaing` (
  `campaing_id` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `campaing`
--

INSERT INTO `campaing` (`campaing_id`) VALUES
(5337780000);

-- --------------------------------------------------------

--
-- Структура таблицы `campaing_orders`
--

CREATE TABLE IF NOT EXISTS `campaing_orders` (
`id` int(11) NOT NULL,
  `campaing_id` varchar(15) NOT NULL,
  `order_id` varchar(20) NOT NULL,
  `status` varchar(500) NOT NULL,
  `cart` float NOT NULL,
  `currency` varchar(100) NOT NULL,
  `action_date` date NOT NULL,
  `details` text NOT NULL,
  `md5` text
) ENGINE=InnoDB AUTO_INCREMENT=39858 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `campaing_orders`
--

INSERT INTO `campaing_orders` (`id`, `campaing_id`, `order_id`, `status`, `cart`, `currency`, `action_date`, `details`, `md5`) VALUES
(34814, '5337780000', '140880733945', 'approved', 3, 'USD', '2016-03-01', '{"comment":[],"currency":"USD","website_name":"Letyshops.ru","status_updated":"2016-03-09 03:44:16","advcampaign_id":"11934","subid1":"-","subid3":"-","subid2":"2016-03-04_15:47:30","subid4":"-","click_date":"2016-03-04 15:47:19","action_id":"49138454","status":"approved","order_id":"140880733945","cart":"3.00","conversion_time":"-316039","payment":"0.20","advcampaign_name":"Ebaysocial","tariff_id":"3388","closing_date":"2016-03-02","subid":"14308285:19485:1453954530_91.211.105.72_126","action_date":"2016-03-01 00:00:00","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437 6"}', '0367cea3d5add5ef6cff634819a6f1af'),
(34815, '5337780000', '73359932626112', 'pending', 1.57, 'USD', '2016-03-01', '{"comment":[],"currency":"USD","website_name":"Letyshops.ru","status_updated":"2016-03-01 23:58:07","advcampaign_id":"6115","subid1":"-","subid3":"-","subid2":"2016-03-01_23:36:16","subid4":"-","click_date":"2016-03-01 23:36:05","action_id":"48559034","status":"pending","order_id":"73359932626112","cart":"1.57","conversion_time":"1322","payment":"0.19","advcampaign_name":"Aliexpress INT","tariff_id":"1954","closing_date":"2016-05-10","subid":"13366481:466724:1456861639_188.237.122.4_440","action_date":"2016-03-01 23:58:07","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437"}', '7f57e9d1651bd2ce7317b2323bdef712'),
(34816, '5337780000', '73367474174272', 'pending', 2.8, 'USD', '2016-03-01', '{"comment":[],"currency":"USD","website_name":"Letyshops.ru","status_updated":"2016-03-01 23:57:56","advcampaign_id":"6115","subid1":"-","subid3":"-","subid2":"2016-03-01_23:54:32","subid4":"-","click_date":"2016-03-01 23:54:22","action_id":"48559027","status":"pending","order_id":"73367474174272","cart":"2.80","conversion_time":"214","payment":"0.34","advcampaign_name":"Aliexpress INT","tariff_id":"1954","closing_date":"2016-05-10","subid":"13366481:348059:1454936094_93.80.22.36_361","action_date":"2016-03-01 23:57:56","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437"}', '16c58c53fb4bee521a0ddb23c371d831'),
(34817, '5337780000', '232118196', 'pending', 364, 'RUB', '2016-03-01', '{"comment":[],"currency":"RUB","website_name":"Letyshops.ru","status_updated":"2016-03-01 23:57:46","advcampaign_id":"14768","subid1":"-","subid3":"-","subid2":"2016-03-01_23:57:16","subid4":"-","click_date":"2016-03-01 23:57:06","action_id":"48559021","status":"pending","order_id":"232118196","cart":"364.00","conversion_time":"40","payment":"19.59","advcampaign_name":"Wildberries CashBack RU","tariff_id":"5361","closing_date":"2016-03-31","subid":"12035058:178735:1455122296_188.234.86.203_680","action_date":"2016-03-01 23:57:46","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437"}', 'cd918f9bdadc0fe899dcab33126c16b0'),
(34818, '5337780000', '232111986', 'pending', 5497, 'RUB', '2016-03-01', '{"comment":[],"currency":"RUB","website_name":"Letyshops.ru","status_updated":"2016-03-01 23:57:46","advcampaign_id":"14768","subid1":"-","subid3":"-","subid2":"2016-03-01_23:34:34","subid4":"-","click_date":"2016-03-01 23:34:24","action_id":"48559015","status":"pending","order_id":"232111986","cart":"5497.00","conversion_time":"1402","payment":"295.74","advcampaign_name":"Wildberries CashBack RU","tariff_id":"5361","closing_date":"2016-03-31","subid":"12035058:228023:1451077654_95.139.4.116_946","action_date":"2016-03-01 23:57:46","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437"}', '3633d73486a6a0749985f0bad917e862'),
(34819, '5337780000', '232114582', 'pending', 173, 'RUB', '2016-03-01', '{"comment":[],"currency":"RUB","website_name":"Letyshops.ru","status_updated":"2016-03-01 23:57:46","advcampaign_id":"14768","subid1":"-","subid3":"-","subid2":"2016-03-01_23:39:20","subid4":"-","click_date":"2016-03-01 23:39:30","action_id":"48559020","status":"pending","order_id":"232114582","cart":"173.00","conversion_time":"1096","payment":"9.31","advcampaign_name":"Wildberries CashBack RU","tariff_id":"5361","closing_date":"2016-03-31","subid":"12035058:262850:1452861331_5.167.231.154_977","action_date":"2016-03-01 23:57:46","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437"}', 'ec3d607c69cd3423007e6f8e60bf68d8'),
(34820, '5337780000', '232112710', 'pending', 338, 'RUB', '2016-03-01', '{"comment":[],"currency":"RUB","website_name":"Letyshops.ru","status_updated":"2016-03-01 23:57:46","advcampaign_id":"14768","subid1":"-","subid3":"-","subid2":"2016-03-01_23:38:25","subid4":"-","click_date":"2016-03-01 23:38:15","action_id":"48559017","status":"pending","order_id":"232112710","cart":"338.00","conversion_time":"1171","payment":"18.19","advcampaign_name":"Wildberries CashBack RU","tariff_id":"5361","closing_date":"2016-03-31","subid":"12035058:262743:1456778576_82.193.140.179_454","action_date":"2016-03-01 23:57:46","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437"}', '84b70476d85023bec3a4de8ee69ab9fa'),
(34821, '5337780000', '232120222', 'pending', 645, 'RUB', '2016-03-01', '{"comment":[],"currency":"RUB","website_name":"Letyshops.ru","status_updated":"2016-03-01 23:57:46","advcampaign_id":"14768","subid1":"-","subid3":"-","subid2":"2016-03-01_23:49:37","subid4":"-","click_date":"2016-03-01 23:49:26","action_id":"48559022","status":"pending","order_id":"232120222","cart":"645.00","conversion_time":"500","payment":"34.71","advcampaign_name":"Wildberries CashBack RU","tariff_id":"5361","closing_date":"2016-03-31","subid":"12035058:312312:1454077358_95.106.182.227_443","action_date":"2016-03-01 23:57:46","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437"}', '255b661e18815ada3402e28ac4f85051'),
(34822, '5337780000', '232112479', 'pending', 10125, 'RUB', '2016-03-01', '{"comment":[],"currency":"RUB","website_name":"Letyshops.ru","status_updated":"2016-03-01 23:57:46","advcampaign_id":"14768","subid1":"-","subid3":"-","subid2":"2016-03-01_23:10:22","subid4":"-","click_date":"2016-03-01 23:10:12","action_id":"48559016","status":"pending","order_id":"232112479","cart":"10125.00","conversion_time":"2854","payment":"544.73","advcampaign_name":"Wildberries CashBack RU","tariff_id":"5361","closing_date":"2016-03-31","subid":"12035058:223745:1456846238_188.19.219.228_888","action_date":"2016-03-01 23:57:46","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437"}', '32b65d9138009c6b2ecbab30a08a88c3'),
(34823, '5337780000', '232102506', 'pending', 335, 'RUB', '2016-03-01', '{"comment":[],"currency":"RUB","website_name":"Letyshops.ru","status_updated":"2016-03-01 23:57:45","advcampaign_id":"14768","subid1":"-","subid3":"-","subid2":"2016-03-01_23:38:25","subid4":"-","click_date":"2016-03-01 23:38:15","action_id":"48559005","status":"pending","order_id":"232102506","cart":"335.00","conversion_time":"1170","payment":"18.03","advcampaign_name":"Wildberries CashBack RU","tariff_id":"5361","closing_date":"2016-03-31","subid":"12035058:262743:1456778576_82.193.140.179_454","action_date":"2016-03-01 23:57:45","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437"}', 'b445261159e51222af913ce89229bce3'),
(34824, '5337780000', '232108958', 'pending', 6434, 'RUB', '2016-03-01', '{"comment":[],"currency":"RUB","website_name":"Letyshops.ru","status_updated":"2016-03-01 23:57:45","advcampaign_id":"14768","subid1":"-","subid3":"-","subid2":"2016-03-01_22:28:07","subid4":"-","click_date":"2016-03-01 22:28:17","action_id":"48559009","status":"pending","order_id":"232108958","cart":"6434.00","conversion_time":"5368","payment":"346.15","advcampaign_name":"Wildberries CashBack RU","tariff_id":"5361","closing_date":"2016-03-31","subid":"12035058:440633:1456855229_5.227.15.101_948","action_date":"2016-03-01 23:57:45","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437"}', '46ffe3e5ee1f4b8abab677881302e4fa'),
(34825, '5337780000', '232110975', 'pending', 3007, 'RUB', '2016-03-01', '{"comment":[],"currency":"RUB","website_name":"Letyshops.ru","status_updated":"2016-03-01 23:57:45","advcampaign_id":"14768","subid1":"-","subid3":"-","subid2":"2016-03-01_23:35:26","subid4":"-","click_date":"2016-03-01 23:35:16","action_id":"48559012","status":"pending","order_id":"232110975","cart":"3007.00","conversion_time":"1349","payment":"161.78","advcampaign_name":"Wildberries CashBack RU","tariff_id":"5361","closing_date":"2016-03-31","subid":"12035058:373645:1455397647_5.141.20.26_181","action_date":"2016-03-01 23:57:45","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437"}', '51a9fcc44ce0ba666da77cec207b9ff2'),
(34826, '5337780000', '232106311', 'pending', 338, 'RUB', '2016-03-01', '{"comment":[],"currency":"RUB","website_name":"Letyshops.ru","status_updated":"2016-03-01 23:57:45","advcampaign_id":"14768","subid1":"-","subid3":"-","subid2":"2016-03-01_23:38:25","subid4":"-","click_date":"2016-03-01 23:38:15","action_id":"48559008","status":"pending","order_id":"232106311","cart":"338.00","conversion_time":"1170","payment":"18.19","advcampaign_name":"Wildberries CashBack RU","tariff_id":"5361","closing_date":"2016-03-31","subid":"12035058:262743:1456778576_82.193.140.179_454","action_date":"2016-03-01 23:57:45","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437"}', '8576a8ef364655ddd22291fab592c3c0'),
(34827, '5337780000', '232109234', 'pending', 2200, 'RUB', '2016-03-01', '{"comment":[],"currency":"RUB","website_name":"Letyshops.ru","status_updated":"2016-03-01 23:57:45","advcampaign_id":"14768","subid1":"-","subid3":"-","subid2":"2016-03-01_23:53:45","subid4":"-","click_date":"2016-03-01 23:53:35","action_id":"48559011","status":"pending","order_id":"232109234","cart":"2200.00","conversion_time":"250","payment":"118.36","advcampaign_name":"Wildberries CashBack RU","tariff_id":"5361","closing_date":"2016-03-31","subid":"12035058:201944:1456821354_178.252.85.147_879","action_date":"2016-03-01 23:57:45","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437"}', '62ef1dbdfe37955b194741b5df55c118'),
(34828, '5337780000', '232111878', 'pending', 4900, 'RUB', '2016-03-01', '{"comment":[],"currency":"RUB","website_name":"Letyshops.ru","status_updated":"2016-03-01 23:57:45","advcampaign_id":"14768","subid1":"-","subid3":"-","subid2":"2016-03-01_23:36:37","subid4":"-","click_date":"2016-03-01 23:36:46","action_id":"48559014","status":"pending","order_id":"232111878","cart":"4900.00","conversion_time":"1259","payment":"263.62","advcampaign_name":"Wildberries CashBack RU","tariff_id":"5361","closing_date":"2016-03-31","subid":"12035058:424996:1456069069_85.192.188.206_220","action_date":"2016-03-01 23:57:45","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437"}', 'b6755765fe85171a83989110d5fee3ee'),
(34829, '5337780000', '232105983', 'pending', 967, 'RUB', '2016-03-01', '{"comment":[],"currency":"RUB","website_name":"Letyshops.ru","status_updated":"2016-03-01 23:57:45","advcampaign_id":"14768","subid1":"-","subid3":"-","subid2":"2016-03-01_23:26:16","subid4":"-","click_date":"2016-03-01 23:26:25","action_id":"48559007","status":"pending","order_id":"232105983","cart":"967.00","conversion_time":"1880","payment":"52.03","advcampaign_name":"Wildberries CashBack RU","tariff_id":"5361","closing_date":"2016-03-31","subid":"12035058:407687:1455879863_109.94.198.99_305","action_date":"2016-03-01 23:57:45","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437"}', 'a562769d890d6158f4a9ebd8cd0efba1'),
(34830, '5337780000', '232109021', 'pending', 169, 'RUB', '2016-03-01', '{"comment":[],"currency":"RUB","website_name":"Letyshops.ru","status_updated":"2016-03-01 23:57:45","advcampaign_id":"14768","subid1":"-","subid3":"-","subid2":"2016-03-01_23:38:25","subid4":"-","click_date":"2016-03-01 23:38:15","action_id":"48559010","status":"pending","order_id":"232109021","cart":"169.00","conversion_time":"1170","payment":"9.10","advcampaign_name":"Wildberries CashBack RU","tariff_id":"5361","closing_date":"2016-03-31","subid":"12035058:262743:1456778576_82.193.140.179_454","action_date":"2016-03-01 23:57:45","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437"}', '27a3fccacd04693fefbf362ca1fd1cca'),
(34831, '5337780000', '232111234', 'pending', 4930, 'RUB', '2016-03-01', '{"comment":[],"currency":"RUB","website_name":"Letyshops.ru","status_updated":"2016-03-01 23:57:45","advcampaign_id":"14768","subid1":"-","subid3":"-","subid2":"2016-03-01_23:36:03","subid4":"-","click_date":"2016-03-01 23:35:53","action_id":"48559013","status":"pending","order_id":"232111234","cart":"4930.00","conversion_time":"1312","payment":"265.24","advcampaign_name":"Wildberries CashBack RU","tariff_id":"5361","closing_date":"2016-03-31","subid":"12035058:266023:1453221675_195.64.208.213_944","action_date":"2016-03-01 23:57:45","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437"}', 'd55b1b796986cacc6a1616519064ba70'),
(34832, '5337780000', '232090625', 'pending', 507, 'RUB', '2016-03-01', '{"comment":[],"currency":"RUB","website_name":"Letyshops.ru","status_updated":"2016-03-01 23:57:44","advcampaign_id":"14768","subid1":"-","subid3":"-","subid2":"2016-03-01_23:53:45","subid4":"-","click_date":"2016-03-01 23:53:35","action_id":"48558993","status":"pending","order_id":"232090625","cart":"507.00","conversion_time":"249","payment":"27.28","advcampaign_name":"Wildberries CashBack RU","tariff_id":"5361","closing_date":"2016-03-31","subid":"12035058:201944:1456821354_178.252.85.147_879","action_date":"2016-03-01 23:57:44","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437"}', '240c8839cd1b543c66557bcd15f8f33c'),
(34833, '5337780000', '232097827', 'pending', 6274, 'RUB', '2016-03-01', '{"comment":[],"currency":"RUB","website_name":"Letyshops.ru","status_updated":"2016-03-01 23:57:44","advcampaign_id":"14768","subid1":"-","subid3":"-","subid2":"2016-03-01_23:13:51","subid4":"-","click_date":"2016-03-01 23:14:01","action_id":"48558997","status":"pending","order_id":"232097827","cart":"6274.00","conversion_time":"2623","payment":"337.55","advcampaign_name":"Wildberries CashBack RU","tariff_id":"5361","closing_date":"2016-03-31","subid":"12035058:400332:1455739028_188.254.53.38_984","action_date":"2016-03-01 23:57:44","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437"}', '1351018ba644c4b356e2f813ef67d811'),
(34834, '5337780000', '232100091', 'pending', 338, 'RUB', '2016-03-01', '{"comment":[],"currency":"RUB","website_name":"Letyshops.ru","status_updated":"2016-03-01 23:57:44","advcampaign_id":"14768","subid1":"-","subid3":"-","subid2":"2016-03-01_23:53:45","subid4":"-","click_date":"2016-03-01 23:53:35","action_id":"48559000","status":"pending","order_id":"232100091","cart":"338.00","conversion_time":"249","payment":"18.19","advcampaign_name":"Wildberries CashBack RU","tariff_id":"5361","closing_date":"2016-03-31","subid":"12035058:201944:1456821354_178.252.85.147_879","action_date":"2016-03-01 23:57:44","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437"}', '9a48dfeb3cfb6b1b9d9233f36945b5d4'),
(34835, '5337780000', '232101438', 'pending', 1161, 'RUB', '2016-03-01', '{"comment":[],"currency":"RUB","website_name":"Letyshops.ru","status_updated":"2016-03-01 23:57:44","advcampaign_id":"14768","subid1":"-","subid3":"-","subid2":"2016-03-01_23:17:27","subid4":"-","click_date":"2016-03-01 23:17:37","action_id":"48559003","status":"pending","order_id":"232101438","cart":"1161.00","conversion_time":"2407","payment":"62.47","advcampaign_name":"Wildberries CashBack RU","tariff_id":"5361","closing_date":"2016-03-31","subid":"12035058:110641:1455184944_5.227.15.195_131","action_date":"2016-03-01 23:57:44","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437"}', '17e36ba507bb0965cf84d0e9e04c2976'),
(34836, '5337780000', '232093916', 'pending', 169, 'RUB', '2016-03-01', '{"comment":[],"currency":"RUB","website_name":"Letyshops.ru","status_updated":"2016-03-01 23:57:44","advcampaign_id":"14768","subid1":"-","subid3":"-","subid2":"2016-03-01_23:38:25","subid4":"-","click_date":"2016-03-01 23:38:15","action_id":"48558996","status":"pending","order_id":"232093916","cart":"169.00","conversion_time":"1169","payment":"9.10","advcampaign_name":"Wildberries CashBack RU","tariff_id":"5361","closing_date":"2016-03-31","subid":"12035058:262743:1456778576_82.193.140.179_454","action_date":"2016-03-01 23:57:44","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437"}', 'd9dfa38e8e680e99719ec351f527e0cf'),
(34837, '5337780000', '232098806', 'pending', 672, 'RUB', '2016-03-01', '{"comment":[],"currency":"RUB","website_name":"Letyshops.ru","status_updated":"2016-03-01 23:57:44","advcampaign_id":"14768","subid1":"-","subid3":"-","subid2":"2016-03-01_20:40:15","subid4":"-","click_date":"2016-03-01 20:40:05","action_id":"48558999","status":"pending","order_id":"232098806","cart":"672.00","conversion_time":"11859","payment":"36.16","advcampaign_name":"Wildberries CashBack RU","tariff_id":"5361","closing_date":"2016-03-31","subid":"12035058:296200:1456747134_46.163.173.71_296","action_date":"2016-03-01 23:57:44","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437"}', 'd2fa8e63e0c54198738bec29bf087b68'),
(34838, '5337780000', '232101248', 'pending', 182, 'RUB', '2016-03-01', '{"comment":[],"currency":"RUB","website_name":"Letyshops.ru","status_updated":"2016-03-01 23:57:44","advcampaign_id":"14768","subid1":"-","subid3":"-","subid2":"2016-03-01_23:20:06","subid4":"-","click_date":"2016-03-01 23:20:16","action_id":"48559002","status":"pending","order_id":"232101248","cart":"182.00","conversion_time":"2248","payment":"9.80","advcampaign_name":"Wildberries CashBack RU","tariff_id":"5361","closing_date":"2016-03-31","subid":"12035058:216768:1450715271_178.72.99.232_759","action_date":"2016-03-01 23:57:44","action":"\\u041e\\u043f\\u043b\\u0430\\u0447\\u0435\\u043d\\u043d\\u044b\\u0439 \\u0437\\u0430\\u043a\\u0430\\u0437"}', '8a0452ba9afa5227b4dc0bb00e933cca');

-- --------------------------------------------------------

--
-- Структура таблицы `campaing_type`
--

CREATE TABLE IF NOT EXISTS `campaing_type` (
`id` int(11) NOT NULL,
  `campaing_id` varchar(20) NOT NULL,
  `type` enum('xml','csv','','') NOT NULL,
  `url` text NOT NULL,
  `md5` text,
  `row` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `campaing_type`
--

INSERT INTO `campaing_type` (`id`, `campaing_id`, `type`, `url`, `md5`, `row`) VALUES
(8, '5337780000', 'csv', 'http://test.work/upload/csv.csv', '248af66e9f8699d492aab9c734cdf3a0', 0),
(9, '5337780000', 'xml', 'http://test.work/upload/xml.xml', 'f964a66d7a5092aaab04b72a90fab3b8', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `campaing_type_fields`
--

CREATE TABLE IF NOT EXISTS `campaing_type_fields` (
`id` int(11) NOT NULL,
  `type_fields` int(11) NOT NULL,
  `campaing_id` varchar(50) NOT NULL,
  `order_id` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL,
  `cart` varchar(50) NOT NULL,
  `currency` varchar(50) NOT NULL,
  `action_date` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `campaing_type_fields`
--

INSERT INTO `campaing_type_fields` (`id`, `type_fields`, `campaing_id`, `order_id`, `status`, `cart`, `currency`, `action_date`) VALUES
(1, 8, '5337780000', 'M', 'C', 'P', 'F', 'B'),
(2, 9, '5337780000', 'order_id', 'status', 'cart', 'currency', 'action_date');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `campaing`
--
ALTER TABLE `campaing`
 ADD UNIQUE KEY `campaing_id` (`campaing_id`), ADD KEY `campaing_id_2` (`campaing_id`);

--
-- Индексы таблицы `campaing_orders`
--
ALTER TABLE `campaing_orders`
 ADD PRIMARY KEY (`id`), ADD KEY `id` (`id`);

--
-- Индексы таблицы `campaing_type`
--
ALTER TABLE `campaing_type`
 ADD PRIMARY KEY (`id`), ADD KEY `id` (`id`);

--
-- Индексы таблицы `campaing_type_fields`
--
ALTER TABLE `campaing_type_fields`
 ADD PRIMARY KEY (`id`), ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `campaing_orders`
--
ALTER TABLE `campaing_orders`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39858;
--
-- AUTO_INCREMENT для таблицы `campaing_type`
--
ALTER TABLE `campaing_type`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `campaing_type_fields`
--
ALTER TABLE `campaing_type_fields`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
