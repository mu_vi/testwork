<?php
defined('BASEPATH') OR exit('No direct script access allowed');
    
class CampaingtypeModel extends CI_Model {

    public $id;
    public $campaing_id;
    public $type;
    public $url;
    public $md5;
    public $row;
    
    function __construct()
    {
        parent::__construct();
        
        $this->load->database();
    }
    
    function setCampaing($value)
    {
        $this->campaing_id = $value;
        
        return $this;
    }
    
    function setId($value)
    {
        $this->id = $value;
                
        return $this;
    }
    
    function setType($value)
    {
        $this->type = $value;
                
        return $this;
    }   
    
    function setUrl($value)
    {
        $this->url = $value;
                
        return $this;
    }
    function setHash($value)
    {
        $this->md5 = $value;
                
        return $this;
    }
    
    function setRow($value)
    {
        $this->row = $value;
                
        return $this;
    }
    
    function findById($cid, $type)
    {
        $this->db->where('type', $type);
        $this->db->where('campaing_id', $cid);
        $query = $this->db->get('campaing_type');
        
        return $query->result();
    }
    
    function update()
    {
        $this->db->where('id', $this->id);       
        
        if(!$this->db->update('campaing_type', $this))
        {
            throw new Exception('The campaing type cannot be update.');
        }
    }
    
    function insert()
    {        
        if(!$this->db->insert('campaing_type', $this))
        {
            throw new Exception('The campaing type cannot be insert.');
        }
    }
}