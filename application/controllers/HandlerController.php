<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HandlerController extends CI_Controller {

	function __construct()
    {
        parent::__construct();
		
		$this->load->model('CampaingModel', 'campaing');        
    }
	
	public function load($cid, $type)
	{
		try{
			$result = $this->campaing
						->getType()
						->findById($cid, $type);
			
			if(!isset($result[0]) || !class_exists($result[0]->type))
			{
				throw new Exception('Class does not exist.');
			}
				
			$result = $result[0];
			
			$model = new $result->type($result->row);
			
			$model->init($result->url);		
			
			if($model->getHashContent() != $result->md5)
			{
				$model->proccess();
				
				$data = $model->get();
				
				$fields = $this->campaing->getFields()->findByTypeId($result->id);
				$fields = (array)$fields[0];		
				
				for($i = 0; $i < count($data); $i++)
				{
					$order = $this->campaing
						->getOrders()
						->findAll(array(
							'order_id' 		=> $data[$i][$fields['order_id']]						
						));				
					
					$object = $this->campaing->getOrders()						
										->setCampaing($cid)
										->setOrder($data[$i][$fields['order_id']])
										->setStatus($data[$i][$fields['status']])
										->setCart($data[$i][$fields['cart']])
										->setCurrency($data[$i][$fields['currency']])
										->setDate($data[$i][$fields['action_date']])
										->setDetails(json_encode($data[$i]))
										->setHash($model->getHashRow($i));
					log_message('debug', "Try update order id by {$data[$i][$fields['order_id']]}");
					
					
					if(!isset($order[0]))
					{
						log_message('debug', "Insert order id by {$data[$i][$fields['order_id']]}");
						
						$object->insert();
					}
					
					if(isset($order[0]) && ($order[0]->md5  != $model->getHashRow($i)))
					{					
						log_message('debug', "Update order id by {$data[$i][$fields['order_id']]}");
	
						$object->update();
					}
				}
				
				$this->campaing
							->getType()
							->setId($result->id)
							->setCampaing($result->campaing_id)
							->setType($result->type)
							->setUrl($result->url)
							->setHash($model->getHashContent())
							->setRow($result->row + $i)
							->update();
			}else{
				log_message('debug', "Campaing {$cid} does not need to be changed");
			}
		}catch(Exception $e){
			log_message('error', 'Exception: ' . $e->getMessage());
		}
	}	
}


