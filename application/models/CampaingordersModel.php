<?php
defined('BASEPATH') OR exit('No direct script access allowed');
    
class CampaingordersModel extends CI_Model
{
    
    public $campaing_id;
    public $order_id;
    public $status;
    public $cart;
    public $currency;
    public $action_date;
    public $details;
    public $md5;    
    
    function __construct()
    {
        parent::__construct();
        
        $this->load->database();
    }
    
    function setCampaing($value)
    {
        $this->campaing_id = $value;
        
        return $this;
    }
    
    function setOrder($value)
    {
        $this->order_id = $value;
        
        return $this;
    }
    
    function setStatus($value)
    {
        $this->status = $value;
        
        return $this;
    }
    
    function setCart($value)
    {
        $this->cart = $value;
        
        return $this;
    }
    
    function setCurrency($value)
    {
        $this->currency = $value;
        
        return $this;
    }
    
    function setDate($value)
    {
        $this->action_date = $value;
        
        return $this;
    }
    
    function setDetails($value)
    {
        $this->details = $value;
        
        return $this;
    }
    
    function setHash($value)
    {
        $this->md5 = $value;
        
        return $this;
    }
    
    
    function findAll($criteria = array())
    {
        foreach($criteria as $key => $value)
        {
            $this->db->where($key, $value);
        }
        
        $query = $this->db->get('campaing_orders');
        
        return $query->result();
    }    
    
    function insert()
    {           
        if(!$this->db->insert('campaing_orders', $this))
        {
            throw new Exception('The campaing orders cannot be insert.');
        }
    }
    
    function update()
    {
        $this->db->where('order_id', $this->order_id);
        
        if(!$this->db->update('campaing_orders', $this))
        {
            throw new Exception('The campaing orders cannot be update.');
        }
    }
}
