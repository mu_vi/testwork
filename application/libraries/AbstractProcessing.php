<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AbstractProcessing
{
    protected $handle;    
    protected $length;    
    protected $delimiter;
    protected $enclosure;
    protected $escape;
    protected $offset;    
    protected $_cell;
    
    protected $url;
    
    protected $hash;
    
    protected $result;
   
    
    function __construct($offset = 0)
    {
        $this->length = 0;
        $this->delimiter = ",";
        $this->enclosure = '"';
        $this->escape = "\\";
        
        $this->offset = $offset;
    }

    function init($url)
    {
        $this->url = $url;        
        $this->handle = fopen($url, 'r');
        
        if(!$this->handle)
        {
            throw new Exception('The file "'.$url.'" cannot be read.');
        }
    }
    
    function getHashContent()
    {
        return md5(file_get_contents($this->url));
    }
    
    function getHashRow($id)
    {
        $this->hash = array_values($this->hash);
        
        return $this->hash[$id];
    }
    
    function get()
    {
        return array_values($this->result);
    }
}