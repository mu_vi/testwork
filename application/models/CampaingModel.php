<?php
defined('BASEPATH') OR exit('No direct script access allowed');
    
class CampaingModel extends CI_Model {

    public $campaing_id;   
    
    function __construct()
    {
        parent::__construct();
        
        $this->load->model('CampaingtypeModel', 'type');
        $this->load->model('CampaingtypefieldsModel', 'fields');
        $this->load->model('CampaingordersModel', 'orders');
        $this->load->database();
    }
    
    function getFields()
    {
        return $this->fields;
    }
    
    function getType()
    {
        return $this->type;
    }
    
    function getOrders()
    {
        return $this->orders;
    }
    
    function setCampaing($value)
    {
        $this->campaing_id = $value;
    }    
    
    function insert()
    {           
        if(!$this->db->insert('campaing', $this))
        {
            throw new Exception('The campaing cannot be insert.');
        }
    }
}