<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once( dirname(__FILE__) . '/AbstractProcessing.php');

class Csv extends AbstractProcessing
{    
    function __construct($offset = 0)
    {
        parent::__construct($offset); 
        
        $this->_cell = self::createColumnsArray('ZZ');
    }    
    
    static function createColumnsArray($end_column, $first_letters = '')
    {
        $columns = array();
        $length = strlen($end_column);
        $letters = range('A', 'Z');
      
        foreach ($letters as $letter)
        {      
            $column = $first_letters . $letter;
      
            $columns[] = $column;
      
            if ($column == $end_column)
                return $columns;
        }
      
        foreach ($columns as $column)
        {      
            if (!in_array($end_column, $columns) && strlen($column) < $length)
            {
                $new_columns = Csv::createColumnsArray($end_column, $column);
      
                $columns = array_merge($columns, $new_columns);
            }
        }
      
        return $columns;
    }

    function proccess()
    {        
        $i = 0;
        $index = 0;
        while($row = fgetcsv($this->handle,
                            $this->length,
                            $this->delimiter,
                            $this->enclosure,
                            $this->escape))
        {
            if($i > $this->offset){
                for($j = 0; $j < count($row); $j++)            
                {               
                    $this->result[$index][$this->_cell[$j]] = $row[$j];                
                }
                
                $this->hash[$index] = md5(serialize($row));
                $index++;
            }
            $i++;
        }        
    }   
}
