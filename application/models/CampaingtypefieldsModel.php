<?php
defined('BASEPATH') OR exit('No direct script access allowed');
    
class CampaingtypefieldsModel extends CI_Model {

    public $type_fields;
    public $campaing_id;
    public $order_id;
    public $status;
    public $cart;
    public $currency;
    public $action_date;
    
    function __construct()
    {
        parent::__construct();
        
        $this->load->database();
    }
    
    function setTypeFields($value)
    {
        $this->type_fields = $value;
    }
    
    function setCampaing($value)
    {
        $this->campaing_id = $value;
    }
    
    function setOrder($value)
    {
        $this->order_id = $value;
    }
    
    function setStatus($value)
    {
        $this->status = $value;
    }
    
    function setCart($value)
    {
        $this->cart = $value;
    }
    
    function setCurrent($value)
    {
        $this->currency = $value;
    }
    
    function setDate($value)
    {
        $this->action_date = $value;
    }
    
    function findByTypeId($fid)
    {
        $this->db->select('order_id, status, cart, currency, action_date');
        $this->db->where('type_fields' , $fid);
        
        $query = $this->db->get('campaing_type_fields');
        
        return $query->result();    
    }
    
    function insert()
    {
        if(!$this->db->insert('campaing_type_fields', $this))
        {
            throw new Exception('The campaing field cannot be insert.');
        }        
    }
}