<?php
defined('BASEPATH') OR exit('No direct script access allowed');

include_once( dirname(__FILE__) . '/AbstractProcessing.php');

class Xml extends AbstractProcessing
{ 
    function proccess(array $data = array(), $index = 0)
    {        
        if(count($data) == 0)
        {
            $data = (array)simplexml_load_file($this->url);
            $data = self::objectToArray($data);
        }
        
        foreach($data as $key => $value)
        {
            if(is_array($data[$key]) && count($data[$key]) > 0)
            {               
                $this->proccess($data[$key], $key);
            }else{
                if($index > $this->offset)
                {                    
                    $this->result[$index][$key] = $value;                
                    $this->hash[$index] = md5(serialize($this->result[$index]));                    
                }
            }
        }        
    }
    
    static function objectToArray($object)
    {
        return json_decode( json_encode($object) , 1);        
    }
}